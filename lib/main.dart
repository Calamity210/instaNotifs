import 'dart:async';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:insta_notifs/api/messaging.dart';
import 'package:insta_notifs/login_page.dart';
import 'package:insta_notifs/pages/first_page.dart';
import 'package:insta_notifs/pages/second_page.dart';
import 'package:insta_notifs/pages/third_page.dart';
import 'package:provider/provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:insta_notifs/auth.dart';

void main() => runApp(
  ChangeNotifierProvider<AuthService>(
    child: MyApp(),
    create: (BuildContext context) {

      return AuthService();
    },
  ),
);

Future<dynamic> _backgroundMessageHandler(Map<String, dynamic> message) async { 
  print("bgbgbgbgbgbgbgbgbgbgbgbgbgbgbgbgbgbgbg");
  await _MyHomePageState._showNotificationWithDefaultSound(message);

  return Future<void>.value();
   }






class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FutureBuilder<FirebaseUser>(
        future: Provider.of<AuthService>(context).getUser(),
        builder: (context, AsyncSnapshot<FirebaseUser> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.error != null) {
              print("error");
              return Text(snapshot.error.toString());
            }
            return snapshot.hasData ? MyHomePage(title: "Cars E Cars", currentUser: snapshot.data) : LoginPage();
          } else {
            // show loading indicator
            return LoadingCircle();
          }
        },
      ),
    );
  }
}

class LoadingCircle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: CircularProgressIndicator(),
        alignment: Alignment(0.0, 0.0),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.currentUser}) : super(key: key);

  

  final String title;
  final FirebaseUser currentUser;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final Firestore _db = Firestore.instance;
  final FirebaseMessaging _fcm = FirebaseMessaging();
  StreamSubscription iosSubscription;
  FirebaseUser fuser;
  List<String> items = ["land1.jpg","land2.jpg","land3.jpg"];

  static final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
 

  @override
  void initState(){
    super.initState();

     var android = new AndroidInitializationSettings('mipmap/ic_launcher');
    var ios = new IOSInitializationSettings();
    var platform = new InitializationSettings(android, ios);
    flutterLocalNotificationsPlugin.initialize(platform,
    onSelectNotification: onSelectNotification);

    _saveDeviceToken();

    _fcmInit();
  }

  _fcmInit() async {

    if (Platform.isIOS) {
      iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
        // save the token  OR subscribe to a topic here
      });

      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }

Future.delayed(Duration(seconds: 1), () {
    _fcm.configure(
      onBackgroundMessage: Platform.isIOS ? null : _backgroundMessageHandler,
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        final notification = message['data'];
        print("titleeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee "+notification['title']);
        await _showNotificationWithDefaultSound(message);

       
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");

        final notification = message['data'];
        handleRouting(notification['title']);

      }, 
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        final notification = message['data'];
        handleRouting(notification['title']);
      },
    );
});
  }



Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
    switch (payload) {
      case '0':
        Navigator.of(context).push(
         MaterialPageRoute(builder: (BuildContext context) => FirstPage()));
        break;
      case '1':
        Navigator.of(context).push(
         MaterialPageRoute(builder: (BuildContext context) => SecondPage()));
        break;

      case '2':
        Navigator.of(context).push(
         MaterialPageRoute(builder: (BuildContext context) => ThirdPage()));
        break;
    }
}  

static Future _showNotificationWithDefaultSound(Map<String, dynamic> msg) async {
    final notification = msg['data'];
  var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      '1', 'your channel name', 'your channel description',
      importance: Importance.Max, priority: Priority.High, enableVibration: true,);
  var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
  var platformChannelSpecifics = new NotificationDetails(
      androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
    1,
    "hhhhhhh",
    notification['body'],
    platformChannelSpecifics,
    payload: notification['title'],
  );
}
  
  

   handleRouting(String payload) {
    switch (payload) {
      case '0':
        Navigator.of(context).push(
         MaterialPageRoute(builder: (BuildContext context) => FirstPage()));
        break;
      case '1':
        Navigator.of(context).push(
         MaterialPageRoute(builder: (BuildContext context) => SecondPage()));
        break;

      case '2':
        Navigator.of(context).push(
         MaterialPageRoute(builder: (BuildContext context) => ThirdPage()));
        break;
    }
  }
  _saveDeviceToken() async {
    // Get the current user
     fuser = await FirebaseAuth.instance.currentUser();

    String uid = fuser.uid;

    // Get the token for this device
    String fcmToken = await _fcm.getToken();


    if (fcmToken != null) {
      var tokens = _db
       .collection('users')
       .document(uid)
       .collection('tokens')
       .document(fcmToken);

      await tokens.setData({
        'token': fcmToken,
        'createdAt': FieldValue.serverTimestamp(), // optional
        'platform': Platform.operatingSystem // optional
      });
    }
  }

  String getToken(String uid){
    _db.collection('users').document(uid).collection('tokens').getDocuments().then((snap){
      return snap.documents[0].documentID;
    });
    return "";
  }

  getUser() async {
    fuser = await FirebaseAuth.instance.currentUser();
    setState(() {});
  }
 
  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: new ListView.builder
        (
       itemCount: items.length,
       itemBuilder: (BuildContext context, int index) {
         return GestureDetector(
           onDoubleTap: (){
             // i do not have the list coming through firebase because i have to preserve my databse, but
             // you should create a method called get fcmToken and inside, I have commented it above
             // save the user uid when they post a new post and pass that on to getToken()
              sendNotification(fuser.uid, "cImPyCbRurk:APA91bHILd7LrOdy0BSht-YGt8FCIIWVGNe_t3EK5xS59wRHugcXIVw97R0C_3cP3nKLw1S4xRrYz40sVNr66r6Odde3JYGeUQ-MZDtfm9oYdPyDlr8yDia8hPTmYsC-xriaf2lCxBxE",index.toString());
           },
          child: Card(
           child: Column(
             children: <Widget>[
               Align(
                 alignment: Alignment.topLeft,
                 child: Text("Mock User",
                 style: TextStyle(
                   fontSize: 16,
                   fontWeight: FontWeight.bold,
                 ),
                 ),
               ),
               Align(
                 alignment: Alignment.center,
                 child: Image.asset("assets/${items[index]}"),
               ),
               Align(
                 alignment: Alignment.bottomCenter,
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                     Container(
                       width: 50,
                       child: Row(
                         children: <Widget>[
                           IconButton(
                             onPressed: (){

                             },
                             icon: Icon(Icons.favorite),
                           ),
                           IconButton(
                             onPressed: (){

                             },
                             icon: Icon(Icons.chat_bubble),
                           ),
                           IconButton(
                             onPressed: (){

                             },
                             icon: Icon(Icons.send),
                           ),
                         ],

                       )
                     ),

                     IconButton(
                       onPressed: (){

                       },
                       icon: Icon(Icons.bookmark),
                     ),
                   ],
                 ),
               )
             ],
           ),

         )
         );
       }
      )
    );
  }

  Future sendNotification(String uid, String fcmToken, String index) async {
    final response = await Messaging.sendTo(
      title: index,
      body: "New Like from $uid",
       fcmToken: "eAig6ncUPK4:APA91bE7l9pGGJzmHZrpfnroNj8yS9kYpNXTWWrAh2lNQoivhQWybnmdPIzyVoe2pUFfhSYzTAGx2vfgv_zm0hhefC2XrXC4sfabTsLerNthrOlKknR2znw4mTj7NS-oohXYtARG0pl4",
    );

    if (response.statusCode != 200) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content:
        Text('[${response.statusCode}] Error message: ${response.body}'),
      ));
    } else {
      print("sent");
    }
  }

  
  
}
