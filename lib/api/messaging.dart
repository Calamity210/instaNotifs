import 'dart:convert';

import 'package:http/http.dart';
import 'package:meta/meta.dart';

class Messaging {
	static final Client client = Client();

	// from 'https://console.firebase.google.com'
	// --> project settings --> cloud messaging --> "Server key"
	static const String serverKey =
		'AAAAfoK2Bz4:APA91bG_8v6sQKxV9KXhSzLYz88ghmddRea84ujFWHe9Dtmn7dAuzW038q7TdeYHDvsFXqxy-O937knI9YXJXN5ygwwn1fZbfmKZMLkgIwi3gb-en7D7lXPEVGVXtSs9OEvM5TVWptrD';

	static Future<Response> sendToAll({
		@required String title,
		@required String body,
	}) =>
		sendToTopic(title: title, body: body, topic: 'all');

	static Future<Response> sendToTopic(
		{@required String title,
			@required String body,
			@required String topic}) =>
		sendTo(title: title, body: body, fcmToken: '/topics/$topic');

	static Future<Response> sendTo({
		@required String title,
		@required String body,
		@required String fcmToken,
	}) =>
		client.post(
			'https://fcm.googleapis.com/fcm/send',
			body: json.encode({
				'priority': 'high',
				'data': {
          'body': '$body',
           'title': '$title',
					'click_action': 'FLUTTER_NOTIFICATION_CLICK',
					'id': '1',
					'status': 'done',
				},
				'to': '$fcmToken',
			}),
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'key=$serverKey',
			},
		);
}