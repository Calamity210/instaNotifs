import 'dart:io';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:path/path.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'auth.dart';


class LoginPage extends StatefulWidget {
	@override
	_LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
	final _formKey = GlobalKey<FormState>();
	String _password;
	String _email;
	String _firstName;
	String _lastName;
	bool _reg = false;

	File _image;

	static final String fileName = Random().nextInt(10000000).toString() + 'jpg';



	Future<void> _uploadFile() async {
		FirebaseUser fUser = await FirebaseAuth.instance.currentUser();

		final StorageReference ref =
		FirebaseStorage.instance.ref().child('ProfilePics').child(fileName);
		final StorageUploadTask uploadTask = ref.putFile(
			_image,
			StorageMetadata(
				contentLanguage: 'en',
				customMetadata: <String, String>{'activity': 'test'},
			),
		);

		final StorageTaskSnapshot downloadUrl =
		(await uploadTask.onComplete);
		final String url = (await downloadUrl.ref.getDownloadURL());

		Firestore.instance.collection('Users').document(fUser.uid).setData({
			'id': fUser.uid,
			'first': _firstName ,
			'last' : _lastName,
			'image': url,
			'ads': 0,
			'views' : 0,
			'replies' : 0
		});
	}


	@override
	Widget build(BuildContext context) {
		if(!_reg) {
			return Scaffold(
				appBar: AppBar(
					title: Text("Login"),
				),
				body: Container(
					padding: EdgeInsets.all(20.0),
					child: Form(
						key: _formKey,
						child: Column(
							mainAxisAlignment: MainAxisAlignment
								.center,
							children: <Widget>[
								SizedBox(height: 20.0),
								Text(
									'Login',
									style: TextStyle(fontSize: 20),
								),
								SizedBox(height: 20.0),
								TextFormField(
									onSaved: (value) =>
									_email = value,
									keyboardType: TextInputType
										.emailAddress,
									decoration: InputDecoration(
										labelText: "Email Address")),
								TextFormField(
									onSaved: (value) =>
									_password = value,
									obscureText: true,
									decoration: InputDecoration(
										labelText: "Password")),

								SizedBox(height: 20.0),
								Row(
									mainAxisAlignment: MainAxisAlignment
										.spaceEvenly,
									children: <Widget>[
										RaisedButton(
											shape: RoundedRectangleBorder(
												borderRadius: BorderRadius
													.all(Radius.circular(25))
											),
											color: Color(0xffb8130b),
											child: Text("REGISTER",
												style: TextStyle(
													color: Colors.white),),
											onPressed: () async {

												setState(() {
													_reg = true;
												});
											},
										),

										RaisedButton(
											shape: RoundedRectangleBorder(
												borderRadius: BorderRadius
													.all(Radius.circular(25))
											),
											color: Color(0xffb8130b),
											child: Text("LOGIN",
												style: TextStyle(
													color: Colors.white),),
											onPressed: () async {
												// save the fields..
												final form = _formKey
													.currentState;
												form.save();

												// Validate will return true if is valid, or false if invalid.
												if (form.validate()) {
													try {
														FirebaseUser result =
														await Provider.of<
															AuthService>(context)
															.loginUser(
															email: _email,
															password: _password);
														print(result);
													} on AuthException catch (error) {
														return _buildErrorDialog(
															context, error.message);
													} on Exception catch (error) {
														return _buildErrorDialog(
															context,
															error.toString());
													}
												}
											},
										)

									],
								)

							])


					)
				));
		} else {
			return Scaffold(
				appBar: AppBar(
					title: Text("Register"),
				),
				body: Container(
					padding: EdgeInsets.all(20.0),
					child: Form(
						key: _formKey,
						child: Column(
							children: <Widget>[
								SizedBox(height: 20.0),
								Text(
									'Register',
									style: TextStyle(fontSize: 20),
								),

								SizedBox(height: 20.0),

								GestureDetector(
									onTap: getImage,
									child: _image  != null ?
									ClipRRect(
										borderRadius: new BorderRadius.circular(60.0),
										child:	Image.file(_image, fit: BoxFit.fill,
											height:120,
											width: 120,)
									)
										:
									new Container(
										height: 120.0,
										width: 120.0,
										decoration: new BoxDecoration(
											color: Colors.grey,
											shape: BoxShape.circle,
										),
									)
								),

								SizedBox(height: 20.0),
								TextFormField(
									onSaved: (value) =>
									_firstName = value,
									keyboardType: TextInputType
										.text,
									decoration: InputDecoration(
										labelText: "First Name")),
								TextFormField(
									onSaved: (value) =>
									_lastName = value,
									keyboardType: TextInputType
										.text,
									decoration: InputDecoration(
										labelText: "Last Name")),
								TextFormField(
									onSaved: (value) =>
									_email = value,
									keyboardType: TextInputType
										.emailAddress,
									decoration: InputDecoration(
										labelText: "Email Address")),
								TextFormField(
									onSaved: (value) =>
									_password = value,
									obscureText: true,
									decoration: InputDecoration(
										labelText: "Password")),

								SizedBox(height: 20.0),
								Row(
									mainAxisAlignment: MainAxisAlignment
										.spaceEvenly,
									children: <Widget>[
										RaisedButton(
											shape: RoundedRectangleBorder(
												borderRadius: BorderRadius
													.all(Radius.circular(25))
											),
											color: Color(0xffb8130b),
											child: Text("LOGIN",
												style: TextStyle(
													color: Colors.white),),
											onPressed: () async {

												setState(() {
													_reg = false;
												});
											},
										),

										RaisedButton(
											shape: RoundedRectangleBorder(
												borderRadius: BorderRadius
													.all(Radius.circular(25))
											),
											color: Color(0xffb8130b),
											child: Text("REGISTER",
												style: TextStyle(
													color: Colors.white),),
											onPressed: () async {
												// save the fields..
												final form = _formKey
													.currentState;
												form.save();

												// Validate will return true if is valid, or false if invalid.
												if (form.validate()) {
													try {
														FirebaseUser result =
														await Provider.of<
															AuthService>(context)
															.signup(_email, _password);
													} on AuthException catch (error) {
														return _buildErrorDialog(
															context, error.message);
													} on Exception catch (error) {
														return _buildErrorDialog(
															context,
															error.toString());
													}
												}

												if(_image == null || _firstName == null || _lastName == null || _email == null || _password == null){
													return _buildErrorDialog(
														context, 'Fill all fields');
												} else{
													_uploadFile();
												}

											},
										)

									],
								)

							])


					)
				));
		}
	}

	Future getImage() async {

		var image = await ImagePicker.pickImage(source: ImageSource.gallery);


		setState(() {
			String _basename = basename(image.path);
			_image = image;
		});

		print(extension);
	}

	Future _buildErrorDialog(BuildContext context, _message) {
		return showDialog(
			builder: (context) {
				return AlertDialog(
					title: Text('Error Message'),
					content: Text(_message),
					actions: <Widget>[
						FlatButton(
							child: Text('Cancel'),
							onPressed: () {
								Navigator.of(context).pop();
							})
					],
				);
			},
			context: context,
		);
	}
}