import 'package:flutter/material.dart';

class FirstPage extends StatelessWidget {
	@override
	Widget build(BuildContext context) => Scaffold(
		appBar: AppBar(title: Text('First page')),
		body:  Card(
				child: Stack(
					children: <Widget>[
						Align(
							alignment: Alignment.topLeft,
							child: Text("Mock User",
								style: TextStyle(
									fontSize: 16,
									fontWeight: FontWeight.bold,
								),
							),
						),
						Align(
							alignment: Alignment.center,
							child: Image.asset("assets/land1.jpg"),
						),
						Align(
							alignment: Alignment.bottomCenter,
							child: Row(
								mainAxisAlignment: MainAxisAlignment.spaceBetween,
								children: <Widget>[
									Container(
										width: 50,
										child: Row(
											children: <Widget>[
												IconButton(
													onPressed: (){

													},
													icon: Icon(Icons.favorite),
												),
												IconButton(
													onPressed: (){

													},
													icon: Icon(Icons.chat_bubble),
												),
												IconButton(
													onPressed: (){

													},
													icon: Icon(Icons.send),
												),
											],

										)
									),

									IconButton(
										onPressed: (){

										},
										icon: Icon(Icons.bookmark),
									),
								],
							),
						)
					],
				),

			)
	);
}